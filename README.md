# IU-lakefinder

## Description

This app has been created as a proof-of-concept app for a school project at IU. The idea of the app is to allow for users to search for lakes in Finland based on geographic coordinates and filtration parameters.
For example I can search for lakes at coordinates Lat: 66.1, Long: 25.5 with search parameters radius, distance to closest buildings and average depth above. 

![Example search result](res1.png)

## Installation

- Tested to work with python version 3.10.12 and 3.12.3
- virtualenv venv to create new virtual environment
- source venv/bin/activate (linux) or .\venv\Scripts\activate (windows)
- Install dependencies using pip3 install -r requirements.txt
- (Optional) To install bigjson, navigate to bigjson_master/bigjson_master and run '''pip3 install -e .'''
- unzip small_dataset.zip into search/data/mapbox_data so that directory contains the following sub-directories: jarvi, rakennusreunaviiva, rapids, roadline, suo, swamp


## Usage

- To run the app locally use uvicorn main:app --host 0.0.0.0 --port 8000 and then navigate to localhost:8000 using your browser. 
- Tested to work with firefox and chrome browsers. 
- The small dataset will only allow you to search in the bounds 68.0 - 69.0, 21.1 - 22.1. Additionally, the swamp data is not included in the small dataset.

