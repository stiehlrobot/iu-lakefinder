

from fastapi import FastAPI, Request, Form, status
from fastapi.responses import JSONResponse, HTMLResponse, RedirectResponse
from fastapi.templating import Jinja2Templates
from search.core import init, run_one_with_maastolakes
from search.util import validated

lake_data, mb_manager = init()

app = FastAPI()
templates = Jinja2Templates(directory="ui")

@app.get("/", response_class=HTMLResponse)
async def redirect():
   
    return RedirectResponse(url='/search')

@app.get("/search", response_class=HTMLResponse)
async def get_data(request: Request):
   
    return templates.TemplateResponse("spandex.html", {"request": request})



@app.post("/search", response_class=HTMLResponse)
async def search(request: Request, gpsLat: str = Form(...),
                 gpsLong: str = Form(...),
                 searchRadius: str = Form(...),
                 distanceToClosestBuilding: str = Form(...),
                 minAverageLakeDepth: str = Form(...),
                 drawSwamps: str = Form(None),
                 drawRoads: str = Form(None)):
    global lake_data, mb_manager
    print(f'Got drawSwamps: {drawSwamps}')
    print(f'Got drawRoads: {drawRoads}')

    if validated(gpsLat, gpsLong, searchRadius, distanceToClosestBuilding, minAverageLakeDepth):
        run_one_with_maastolakes(lake_data, mb_manager, float(searchRadius), float(gpsLat), float(gpsLong), float(minAverageLakeDepth), float(distanceToClosestBuilding), drawSwamps, drawRoads)
        return templates.TemplateResponse("testmap.html", context={"request": request})
    else:
        redirect_url = str(request.url_for('index'))+ '?x-error=Invalid+params'
        return RedirectResponse(redirect_url, status_code=status.HTTP_302_FOUND, headers={"x-error": "Invalid search params"})
