"""
Contains Api clients and data converters for api data
"""

import requests
import sys
import json
from datetime import datetime
from pathlib import Path
import time 
from time import perf_counter

from .map_objects import Point
from .settings import PATH_BASE
from .map_objects import Lake



class LakeConverter:

    """This class is the main data interface for lake data, internally it fetches lake data using lake api"""

    def __init__(self):
        self.keys = {"id" : "Jarvi_Id", "number": "Nro", "name": "Nimi", "lat": "KoordErLat",
                     "long": "KoordErLong", "municipality_name":"KuntaNimi", "water_area_size_ha":"Vesiala",
                    "coastline_length": "Rantaviiva", "depth_average_m":"SyvyysKeski", 
                    "depth_max_m":"SyvyysSuurin", "water_volume_m3": "Tilavuus", 
                    "depth_max_lat":"SyvyysSuurinKoordErLat", "depth_max_long":"SyvyysSuurinKoordErLong"}

        pass

    def convert_to_lake_data(self, json_data) -> Lake:
        l = Lake()
        for attrib in list(Lake.__annotations__.keys()):
            if attrib in ("id"):
                setattr(l, attrib, int(json_data[self.keys[attrib]]))
            elif attrib in ("name", "municipality_name", "number"):
                setattr(l, attrib, json_data[self.keys[attrib]])
            elif attrib in ("location"):
                setattr(l, attrib, Point(float(json_data[self.keys["lat"]]), float(json_data[self.keys["long"]])))
            else:
                if json_data[self.keys[attrib]] != None:
                    setattr(l, attrib, float(json_data[self.keys[attrib]]))
        return l


class DataStore:

    def __init__(self, data_converter=None):
        self.fname = PATH_BASE /  'data' / 'lakedata_combined.json'
        self.converter = data_converter
        pass

    def timestamp(self):
        return datetime.now().strftime("%d-%m-%YT%H:%M:%S")
    
    def empty(self):
        try:
            with open(self.fname, 'w') as fout:
                fout.write("")
        except IOError as err:
            print(err)

    def save(self, data: str):
        """Saves a json file"""
        try:
            
            with open(self.fname, 'a') as fout:
               
                fout.write(data)
                fout.write('\n')
        except IOError as err:
            print(err)

    def save_json(self, fname, data):
        try:
            
            with open(fname, 'a') as fout:               
                fout.write(data)
        except IOError as err:
            print(err)

    def load(self) -> list[Lake]:
        data = []
        try:
            t0 = perf_counter()
            with open(self.fname) as f:
                for line in f:
                    if self.converter:
                        lakedata = self.converter.convert_to_lake_data(json.loads(line))
                        data.append(lakedata)

            t1 = perf_counter()
            print(f'Loading data took: {t1-t0} seconds')
        except IOError as err:
            print(err)

        print(f'Loaded data size bytes: {sys.getsizeof(data)}')
        return data


class LakeApiClient:

    def __init__(self):
        self.base_url = 'https://rajapinnat.ymparisto.fi/api/jarvirajapinta/1.0/odata/'
        self.chunk_path = 'http://rajapinnat.ymparisto.fi/api/jarvirajapinta/1.0/odata/Jarvi?$skip='

    def get_single_data(self, path: str) -> tuple[int, str]:
        path = self.base_url + path      

       
        try:
            resp = requests.get(path)
        except requests.exceptions.Timeout:
            print("Error: Got timeout")
        except requests.exceptions.TooManyRedirects:
            print("Bad url")
        except requests.exceptions.RequestException as e:
            raise SystemExit(e)
        #print(f"Status code: {resp.status_code}")
        return json.loads(resp.text)
    
    def get_data_chunk(self, chunksize):
        path = self.chunk_path + str(chunksize)
        print(f'PATH: {path}')
        
        try:
            resp = requests.get(path)
        except requests.exceptions.Timeout:
            print("Error: Got timeout")
        except requests.exceptions.TooManyRedirects:
            print("Bad url")
        except requests.exceptions.RequestException as e:
            raise SystemExit(e)
        #print(f"Status code: {resp.status_code}")
        return resp.text


class BuildingApiClient:

    def __init__(self):
        self.initial_path = "https://avoin-paikkatieto.maanmittauslaitos.fi/maastotiedot/features/v1/collections/rakennusreunaviiva/items?limit=1000&f=json"
        self.ids = [] 
    
    def has_duplicates(self):
        return len(self.ids) != len(set(self.ids))

    def send_initial_request(self):
        try:
            payload = {'api-key':'6fd2d076-c477-4d20-b308-721f59663940'}
            resp = requests.get(self.initial_path, params=payload)
        except requests.exceptions.Timeout:
            print("Error: Got timeout")
        except requests.exceptions.TooManyRedirects:
            print("Bad url")
        except requests.exceptions.RequestException as e:
            raise SystemExit(e)
        return resp

    def get_data_as_chunks(self, chunks):
        
        resp = self.send_initial_request()
        jsond = json.loads(resp.text)
        #get the ids
        for i in range(1000):
            self.ids.append(jsond['features'][i]['id'])
        p = Path('mapbox_json') / f'rakennusreunaviiva_chunk_0'
        with open(p, 'w') as fout:
            fout.write(resp.text)

        next_page_url = jsond['links'][3]['href']
            
        for i in range(1,chunks+1):
            print(f"Processing: {i} / {chunks}")
            try:
                resp = requests.get(next_page_url)
                p = Path('mapbox_json') / f'rakennusreunaviiva_chunk_{i}'
                with open(p, 'w') as fout:
                    fout.write(resp.text)
                jsond = json.loads(resp.text)

                #get the ids
                for i in range(1000):
                    self.ids.append(jsond['features'][i]['id'])

                next_page_url = jsond['links'][3]['href']
                time.sleep(2)
                
            except requests.exceptions.Timeout:
                print("Error: Got timeout")
            except requests.exceptions.TooManyRedirects:
                print("Bad url")
            except requests.exceptions.RequestException as e:
                raise SystemExit(e)
            print(resp.status_code)
            #print(resp.text)
            jsond = json.loads(resp.text)
            print(jsond['links'][3]['href'])
    
    def get_data_chunk(self, chunksize):
        path = self.chunk_path + str(chunksize)
        print(f'PATH: {path}')
        
        try:
            resp = requests.get(path)
        except requests.exceptions.Timeout:
            print("Error: Got timeout")
        except requests.exceptions.TooManyRedirects:
            print("Bad url")
        except requests.exceptions.RequestException as e:
            raise SystemExit(e)
        #print(f"Status code: {resp.status_code}")
        return resp.text


class MaastotiedotApiClient:

    def __init__(self, feature_type, api_call_interval_s=3):
        if feature_type not in ('aallonmurtaja', 'aidansymboli', 'aita', 'allas', 'aluemerenulkoraja',
                                'ampumaalue', 'ankkuripaikka', 'autoliikennealue', 'harvalouhikko',
                                'hautausmaa', 'hietikko', 'hylky', 'hylynsyvyys', 'ilmaradankannatinpylvas',
                                'ilmarata', 'jarvi',  'jyrkanne',  'kaatopaikka', 'kaislikko', 'kallioalue',
                                'kalliohalkeama',  'kalliosymboli',  'kellotapuli', 'kivi', 'kivikko', 'korkeuskayra',
                                'korkeuskayrankorkeusarvo',  'koski', 'kunnanhallintokeskus', 'kunnanhallintoraja',
                                'kunta',  'lahde',  'lahestymisvalo',  'lentokenttalue', 'louhos','luiska',
                                'luonnonpuisto',  'luonnonsuojelualue', 'maaaineksenottoalue',  'maasto2kuvionreuna',
                                'maastokuvionreuna', 'maatalousmaa', 'maatuvavesialue', 'masto', 'mastonkorkeus',
                                'matalikko', 'meri',  'merkittavaluontokohde', 'metsamaankasvillisuus', 'metsamaanmuokkaus',
                                'metsanraja', 'muistomerkki', 'muuavoinalue', 'muuntaja', 'muuntoasema', 'nakotorni',
                                'niitty', 'osoitepiste', 'paikannimi', 'pato', 'pelastuskoodipiste', 'pistolaituriviiva',
                                'portti', 'puu', 'puurivi', 'rajavyohykkeenjakaja', 'rakennelma', 'rakennus',
                                'rakennusreunaviiva', 'rauhoitettukohde', 'rautatie', 'rautatieliikennepaikka',
                                'rautatiensymboli', 'retkeilyalue', 'sahkolinja', 'sahkolinjansymboli', 'Satama-alue',
                                'savupiippu', 'savupiipunkorkeus', 'selite', 'sisaistenaluevesienulkoraja', 'soistuma',
                                'sulkuportti', 'suo', 'suojaalue', 'suojaalueenreunaviiva', 'suojametsa',
                                'suojametsanreunaviiva', 'suurjannitelinjanpylvas','syvyyskayra', 'syvyyskayransyvyysarvo',
                                'syvyyspiste', 'taajaanrakennettualue', 'taajaanrakennetunalueenreuna','taytemaa',
                                'tervahauta', 'tienroteksti', 'tiesymboli', 'tieviiva', 'tulentekopaikka', 'tulvaalue',
                                'tunnelinaukko', 'turvalaite', 'tuulivoimala', 'uittolaite', 'uittoranni', 
                                'ulkojasisasaaristonraja', 'urheilujavirkistysalue', 'valtakunnanrajapyykki', 'varastoalue',
                                'vedenpinnankorkeusluku', 'vesiasteikko', 'vesikivi', 'vesikivikko', 'vesikulkuvayla',
                                'vesikulkuvaylankulkusuunta', 'vesikulkuvaylanteksti', 'vesikuoppa', 'vesitorni',
                                'viettoviiva', 'virtausnuoli', 'virtavesialue', 'virtavesikapea'):
            raise KeyError("Key not in allowed feature list!")
        self.call_interval = api_call_interval_s
        self.feature = feature_type
        self.initial_path = f"https://avoin-paikkatieto.maanmittauslaitos.fi/maastotiedot/features/v1/collections/{self.feature}/items?limit=1000&f=json"
        self.ids = [] 
    
    def has_duplicates(self):
        return len(self.ids) != len(set(self.ids))

    def send_initial_request(self):
        try:
            payload = {'api-key':'6fd2d076-c477-4d20-b308-721f59663940'}
            resp = requests.get(self.initial_path, params=payload)
        except requests.exceptions.Timeout:
            print("Error: Got timeout")
        except requests.exceptions.TooManyRedirects:
            print("Bad url")
        except requests.exceptions.RequestException as e:
            raise SystemExit(e)
        return resp

    def get_data_as_chunks(self, chunks):
        resp = self.send_initial_request()
        jsond = json.loads(resp.text)
        #get the ids
        try:
            for i in range(1000):
                self.ids.append(jsond['features'][i]['id'])
        except IndexError:
            print("IndexError: most likely on final page.")
            pass
        p = Path('mapbox_json') / f'{self.feature}' / f'{self.feature}_chunk_0'
        with open(p, 'w+') as fout:
            fout.write(resp.text)

        next_page_url = jsond['links'][3]['href']
            
        for i in range(1,chunks+1):
            print(f"Processing: {i} / {chunks}")
            try:
                resp = requests.get(next_page_url)
                p = Path('mapbox_json') / f'{self.feature}' / f'{self.feature}_chunk_{i}'
                with open(p, 'w+', encoding='utf-8') as fout:
                    fout.write(resp.text)
                jsond = json.loads(resp.text)

                #get the ids
                try:
                    for i in range(1000):
                        self.ids.append(jsond['features'][i]['id'])
                except IndexError:
                    print("Most likely on final page.")
                    pass

                next_page_url = jsond['links'][3]['href']
                time.sleep(self.call_interval)
                
            except requests.exceptions.Timeout:
                print("Error: Got timeout")
            except requests.exceptions.TooManyRedirects:
                print("Bad url")
            except requests.exceptions.RequestException as e:
                raise SystemExit(e)
            print(resp.status_code)
            #print(resp.text)
            jsond = json.loads(resp.text)
            print(jsond['links'][3]['href'])
    
    def get_data_chunk(self, chunksize):
        path = self.chunk_path + str(chunksize)
        print(f'PATH: {path}')
        
        try:
            resp = requests.get(path)
        except requests.exceptions.Timeout:
            print("Error: Got timeout")
        except requests.exceptions.TooManyRedirects:
            print("Bad url")
        except requests.exceptions.RequestException as e:
            raise SystemExit(e)
        #print(f"Status code: {resp.status_code}")
        return resp.text



class LakeManager:

    def __init__(self, ApiClient, Converter, data_store):
        self.lake_api = ApiClient
        self.converter = Converter
        self.storage = data_store

    def refresh_lake_data(self, start_id, end_id):
        self.storage.empty()
        for lake_id in range(start_id, end_id):
            jsond = self.lake_api.get_data(f'Jarvi({lake_id})')
            self.storage.save(self.converter.convert_to_lake_data(jsond))
            if lake_id % 100 == 0:
                print(f'Status: {lake_id} / {end_id}')
                time.sleep()
    
    def refresh_lake_data_by_chunks(self, max):
        self.storage.empty()
        for i in range(0, max, 500):
            jsond = self.lake_api.get_data_chunk(i)
            fname = Path('data') / f'lakedata_{str(i)}.json'
            self.storage.save_json(fname, jsond)

            print(f'Status: {i} / {max}')
            time.sleep(3)



def get_lake_data():
    lama = LakeConverter()
    dstore = DataStore(lama)
    
    return dstore.load()


def populate_lake_data():

    lapi = LakeApiClient()
    
    lstore = DataStore()

    man = LakeManager(lapi, lama, lstore)
    man.refresh_lake_data_by_chunks(65356)

def get_full_data_as_chunks():

    b = BuildingApiClient()
    try:
        b.get_data_as_chunks(5500)
    except Exception as err:
        print(err)
    print(f'HAS DUPLICATES: {b.has_duplicates()}')


def get_full_lake_data_as_chunks():

    m = MaastotiedotApiClient('jarvi')
    try:
        m.get_data_as_chunks(180)
    except Exception as err:
        print(err)
    print(f'HAS DUPLICATES: {m.has_duplicates()}')

def get_full_rapids_data_as_chunks():

    m = MaastotiedotApiClient('koski')
    try:
        m.get_data_as_chunks(1000)
    except Exception as err:
        print(err)
    print(f'HAS DUPLICATES: {m.has_duplicates()}')


def get_full_swamps_data_as_chunks():

    m = MaastotiedotApiClient('suo', 10)
    try:
        m.get_data_as_chunks(5000)
    except Exception as err:
        print(err)
    print(f'HAS DUPLICATES: {m.has_duplicates()}')


def get_full_roadlines_data_as_chunks():

    m = MaastotiedotApiClient('tieviiva', 10)
    try:
        m.get_data_as_chunks(10000)
    except Exception as err:
        print(err)
    print(f'HAS DUPLICATES: {m.has_duplicates()}')

"""
#get_full_roadlines_data_as_chunks()
d = DataStore(LakeConverter())
lakes = d.load()
print(len(lakes))
"""