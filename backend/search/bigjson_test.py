"""
Module contains
"""


import bigjson
from pathlib import Path
from time import perf_counter
import os
import json

from map_objects import Building, Point, MaastoLake, Rapids, Swamp, RoadLine
from map_matrix import MapBoxManager
from settings import PATH_BASE
from util import get_average

fname = PATH_BASE / 'search' / 'data' / 'rakennusreunaviiva_bulk.json'

def populate_mapbox_data(chunk_start: int, chunk_end: int, map_box_manager: MapBoxManager):
    """A building data loader that handles large .json files as stream and populates mapboxes with building data"""
    for i in range(chunk_start, chunk_end):
        
        print(f"NEW BUILDING : {i+1} / {chunk_end}")
        with open(fname, 'rb') as f:            
                
            j = bigjson.load(f)        
            
            coords = list(j['features'][i]['geometry']['coordinates'])
            avg_lat, avg_long = get_average(coords)        
            
            pointlist = []
            for c in coords:
                long, lat = tuple(c)
                pointlist.append(Point(lat, long))              
            
            matching = map_box_manager.get_matching(Point(avg_lat, avg_long))
            if matching:
                matching = map_box_manager.get_matching(Point(avg_lat, avg_long))
                #print(f"Found match: {avg_long} in {matching.long_range} AND {avg_lat} in {matching.lat_range}")
                matching.append_building_to_file(Building(Point(avg_lat, avg_long), pointlist))            
            else:
                print(f"No match for: {avg_lat}, {avg_long}")


def populate_mapbox_data(chunk_start: int, chunk_end: int, map_box_manager: MapBoxManager):
    """A building data loader that handles large .json files as stream and populates mapboxes with building data"""
    for i in range(chunk_start, chunk_end):
        
        print(f"NEW ITEM : {i+1} / {chunk_end}")
        with open(fname, 'rb') as f:            
                
            j = bigjson.load(f)        
            
            coords = list(j['features'][i]['geometry']['coordinates'])
            avg_lat, avg_long = get_average(coords)        
            
            pointlist = []
            for c in coords:
                long, lat = tuple(c)
                pointlist.append(Point(lat, long))              
            
            matching = map_box_manager.get_matching(Point(avg_lat, avg_long))
            if matching:
                matching = map_box_manager.get_matching(Point(avg_lat, avg_long))
                print(f"Found match: {avg_long} in {matching.long_range} AND {avg_lat} in {matching.lat_range}")
                #matching.append_building_to_file(Building(Point(avg_lat, avg_long), pointlist))            
            else:
                print(f"No match for: {avg_lat}, {avg_long}")


def populate_mapbox_from_json(map_box_manager, feature_name):
    """Loads building data from regular .json files"""
    #get all filenames using glob or something
    files = [f for f in os.listdir('mapbox_json') if f.startswith(feature_name)]

    #iterate files
    for num, f in enumerate(files):
        fp = Path('mapbox_json') / f
        with open(fp, 'rb') as fin:
            fdata = json.loads(fin.read())
            for i in range(0, 999):
                print(f"Processing file: {num} of {len(files)}")

                #extract geometry coordinates
                coords = list(fdata['features'][i]['geometry']['coordinates'])

                #calculate average co-ordinates
                avg_lat, avg_long = get_average(coords)
                pointlist = []
                for c in coords:
                    long, lat = tuple(c)
                    pointlist.append(Point(lat, long))
                
                #get a matching mapbox                  
                matching = map_box_manager.get_matching(Point(avg_lat, avg_long))
                if matching:
                    matching = map_box_manager.get_matching(Point(avg_lat, avg_long))
                    print(f"Found match: {avg_long} in {matching.long_range} AND {avg_lat} in {matching.lat_range}")
                    #matching.append_building_to_file(Building(Point(avg_lat, avg_long), pointlist))            
                else:
                    print(f"No match for: {avg_lat}, {avg_long}")


def populate_mapbox_building_from_json(map_box_manager, feature_name):
    """Loads building data from regular .json files"""
    #get all filenames using glob or something
    files = [f for f in os.listdir('mapbox_json') if f.startswith(feature_name)]

    #iterate files
    for num, f in enumerate(files):
        fp = Path('mapbox_json') / f
        with open(fp, 'rb') as fin:
            fdata = json.loads(fin.read())
            for i in range(0, 999):
                print(f"Processing file: {num} of {len(files)}")

                #extract geometry coordinates
                coords = list(fdata['features'][i]['geometry']['coordinates'])

                #calculate average co-ordinates
                avg_lat, avg_long = get_average(coords)
                pointlist = []
                for c in coords:
                    long, lat = tuple(c)
                    pointlist.append(Point(lat, long))
                
                #get a matching mapbox                  
                matching = map_box_manager.get_matching(Point(avg_lat, avg_long))
                if matching:
                    matching = map_box_manager.get_matching(Point(avg_lat, avg_long))
                    print(f"Found match: {avg_long} in {matching.long_range} AND {avg_lat} in {matching.lat_range}")
                    #matching.append_building_to_file(Building(Point(avg_lat, avg_long), pointlist))            
                else:
                    print(f"No match for: {avg_lat}, {avg_long}")    


def populate_mapbox_lake_from_json(map_box_manager, feature_name, start_file_position=0):
    """Loads building data from regular .json files"""
    #get all filenames using glob or something
    files = [f for f in os.listdir('mapbox_json') if f.startswith(feature_name)][start_file_position:]
    unmatched = 0
    #iterate files
    for num, f in enumerate(files):
        fp = Path('mapbox_json') / f
        with open(fp, 'rb') as fin:
            fdata = json.loads(fin.read())
            for i in range(0, 999):
                print(f"Processing file: {num} of {len(files)}")

                errors=10
                #extract geometry coordinates
                try:
                    coords = list(fdata['features'][i]['geometry']['coordinates'][0])
                    errors+=1
                except IndexError:
                    print('coordinates Index [0] at item: ')
                    #print(fdata['features'][i]['geometry']['coordinates'])
                    errors-=1
                    continue
                    
                if errors == 0:
                    print("Too many index errors with coordinates. Breaking loop..")
                    break

                #calculate average co-ordinates
                point_data = fdata['features'][i]['properties']['sijainti_piste']['coordinates']
                long = point_data[0]
                lat = point_data[1]

                pointlist = []
                
                for c in coords:
                    
                    long, lat = tuple(c)
                    pointlist.append(Point(lat, long))
                
                #get a matching mapbox                  
                matching = map_box_manager.get_matching(Point(lat, long))
                if matching:
                    #print(f"Found match: {avg_long} in {matching.long_range} AND {avg_lat} in {matching.lat_range}")

                    lake = MaastoLake(Point(lat, long), pointlist)
                    matching.append_lake_to_file(lake)            
                else:
                    print(f"No match for: {lat}, {long}")
                    unmatched+=1

    print(f'Finished with {unmatched} unmatched lake records.')

def populate_mapbox_rapids_from_json(map_box_manager, feature_name, start_file_position=0):
    """Loads building data from regular .json files"""
    #get all filenames using glob or something
    p = Path('mapbox_json') / feature_name
    files = [f for f in os.listdir(p) if f.startswith(feature_name)][start_file_position:]
    unmatched = 0
    #iterate files
    for num, f in enumerate(files):
        fp = p / f
        with open(fp, 'rb') as fin:
            fdata = json.loads(fin.read())
            for i in range(0, 999):
                print(f"Processing file: {num} of {len(files)}")

                errors=10
                #extract geometry coordinates
                try:
                    coords = list(fdata['features'][i]['geometry']['coordinates'])
                    errors+=1
                except IndexError:
                    print('coordinates Index [0] at item: ')
                    #print(fdata['features'][i]['geometry']['coordinates'])
                    errors-=1
                    continue
                    
                if errors == 0:
                    print("Too many index errors with coordinates. Breaking loop..")
                    break

                print(coords)
                #calculate average co-ordinates
                #calculate average co-ordinates
                avg_lat, avg_long = get_average(coords)
                
                pointlist = []
                
                
                for c in coords:
                    
                    long, lat = tuple(c)
                    pointlist.append(Point(lat, long))
                
                #get a matching mapbox                  
                matching = map_box_manager.get_matching(Point(avg_lat, avg_long))
                if matching:
                    #print(f"Found match: {avg_long} in {matching.long_range} AND {avg_lat} in {matching.lat_range}")

                    rapids = Rapids(Point(avg_lat, avg_long), pointlist)
                    matching.append_rapids_to_file(rapids)            
                else:
                    print(f"No match for: {lat}, {long}")
                    unmatched+=1

    print(f'Finished with {unmatched} unmatched rapids records.')   


def populate_mapbox_swamp_from_json(map_box_manager, feature_name, start_file_position=0):
    """Loads building data from regular .json files"""
    #get all filenames using glob or something
    p = Path('mapbox_json') / feature_name
    files = [f for f in os.listdir(p) if f.startswith('suo')][start_file_position:]
    print(files)
    unmatched = 0
    #iterate files
    for num, f in enumerate(files):
        fp = p / f
        print(fp)
        with open(fp, 'rb') as fin:
            
            fdata = json.loads(fin.read())
            for i in range(0, 999):
                print(f"Processing file: {num} of {len(files)}")

                errors=10
                #extract geometry coordinates
                try:
                    coords = list(fdata['features'][i]['geometry']['coordinates'][0])
                    errors+=1
                except IndexError:
                    print('Index error at geometry coordinates ')
                    #print(fdata['features'][i]['geometry']['coordinates'])
                    errors-=1
                    continue
                    
                if errors == 0:
                    print("Too many index errors with coordinates. Breaking loop..")
                    break
                try:
                    #calculate kulkukelpoisuus
                    traversability_class = fdata['features'][i]['properties']['kulkukelpoisuus']
                except IndexError:
                    print(f'Index error at traversability class extraction')
                    continue

                #forest_class
                try:
                    #calculate kulkukelpoisuus
                    forest_class = fdata['features'][i]['properties']['metsaisyys']
                except IndexError:
                    print(f'Index error at forest class extraction')
                    continue
                
                #calculate average co-ordinates
                point_data = fdata['features'][i]['properties']['sijainti_piste']['coordinates']
                long = point_data[0]
                lat = point_data[1]

                pointlist = []
                
                for c in coords:
                    
                    long, lat = tuple(c)
                    pointlist.append(Point(lat, long))
                
                #get a matching mapbox                  
                matching = map_box_manager.get_matching(Point(lat, long))
                if matching:
                    #print(f"Found match: {avg_long} in {matching.long_range} AND {avg_lat} in {matching.lat_range}")

                    swamp = Swamp(Point(lat, long), pointlist, int(traversability_class), int(forest_class))
                    matching.append_map_object_to_file(swamp)            
                else:
                    print(f"No match for: {lat}, {long}")
                    unmatched+=1

    print(f'Finished with {unmatched} unmatched swamp records.')


def populate_mapbox_roadline_from_json(map_box_manager, feature_name, start_file_position=0):
    """Loads building data from regular .json files"""
    #get all filenames using glob or something
    p = Path('mapbox_json') / feature_name
    print(p)
    files = [f for f in os.listdir(p) if f.startswith(feature_name)][start_file_position:]
    
    unmatched = 0
    #iterate files
    for num, f in enumerate(files):
        fp = p / f
        print(fp)
        with open(fp, 'rb') as fin:
            
            fdata = json.loads(fin.read())
            for i in range(0, 999):
                print(f"Processing file: {num} of {len(files)}")

                errors=10
                #extract geometry coordinates
                try:
                    coords = list(fdata['features'][i]['geometry']['coordinates'])
                    errors+=1
                except IndexError:
                    print('Index error at geometry coordinates ')
                    errors-=1
                    continue
                    
                if errors == 0:
                    print("Too many index errors with coordinates. Breaking loop..")
                    break
                try:
                    traverse_method = fdata['features'][i]['properties']['kulkutapa']
                except IndexError:
                    print(f'Index error at traverse method')
                    continue
                
                try:                    
                    covering = fdata['features'][i]['properties']['paallyste']
                except IndexError:
                    print(f'Index error at covering')
                    continue
                    
                try:                    
                    one_directional = fdata['features'][i]['properties']['yksisuuntaisuus']
                except IndexError:
                    print(f'Index error at covering')
                    continue

                try:                    
                    name_finnish = fdata['features'][i]['properties']['nimi_suomi']
                except IndexError:
                    print(f'Index error finnish name')
                    continue
                
                #calculate average co-ordinates
                point_data = fdata['features'][i]['geometry']['coordinates']
                avg_lat, avg_long = get_average(coords)
                

                pointlist = []
                
                for c in coords:
                    
                    long, lat = tuple(c)
                    pointlist.append(Point(lat, long))
                
                #get a matching mapbox                  
                matching = map_box_manager.get_matching(Point(avg_lat, avg_long))
                if matching:

                    roadline = RoadLine(Point(avg_lat, avg_long), pointlist, int(traverse_method), int(covering), int(one_directional), name_finnish)                   
                    matching.append_map_object_to_file(roadline)            
                else:
                    print(f"No match for: {lat}, {long}")
                    unmatched+=1

    print(f'Finished with {unmatched} unmatched swamp records.')

def timed_population():
    """Time utility function to populate mapboxes from json"""
    m = MapBoxManager(0.25)
    t0 = perf_counter()

    populate_mapbox_from_json(m)

    t1 = perf_counter()
    print(f'Items: - Elapsed time: {(t1-t0)/60} min ')

"""
data = None
with open(Path('mapbox_json') / 'tieviiva' / 'tieviiva_chunk_0', 'rb') as f:
    data = f.read()
jsond = json.loads(data)
print(jsond['features'][0].keys())
print(jsond['features'][0]['id'])
print(jsond['features'][0]['geometry']['coordinates'])
#print(jsond['features'][0]['properties']['sijainti_piste']['coordinates'])
"""



#m = MapBoxManager()
#populate_mapbox_roadline_from_json(m, 'tieviiva', 1)




