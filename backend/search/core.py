"""Core loop for program"""

from folium.plugins import Draw
from .api_connector import get_lake_data
from .map import FinalMap

from .util import make_bounding_box, distance_km_between_points, validated, lakes_in_radius, timeit

from .map_objects import Point, Building, Lake, MaastoLake, Rapids, Swamp, RoadLine
from .map_matrix import MapBoxManager, MapBox
from .settings import PATH_BASE
from typing import List, Tuple

from time import perf_counter
import numpy as np
from threading import Thread


def get_lakes_in_radius_km(lakes: List[Lake], src: Point, radius_km) -> List[Lake]:
    """Returns a list of lakes in a speciefied km radius"""

    resultset = []
    for l in lakes:
        if l.lat is not None and l.long is not None:
            if distance_km_between_points(src, Point(l.lat, l.long)) <= float(radius_km):
                resultset.append(l)
    return resultset

def get_swamps_in_radius_km(swamps: List[Swamp], src: Point, radius_km) -> List[Lake]:
    """Returns a list of lakes in a speciefied km radius"""

    resultset = []
    for s in swamps:
        if s.location.lat is not None and s.location.long is not None:
            if distance_km_between_points(src, Point(s.location.lat, s.location.long)) <= float(radius_km):
                resultset.append(s)
    return resultset


def get_maastolakes_in_radius_km(lakes: List[MaastoLake], src: Point, radius_km) -> List[Lake]:
    """Returns a list of lakes in a speciefied km radius"""

    resultset = []
    for l in lakes:
        if l.location.lat is not None and l.location.long is not None:
            if distance_km_between_points(src, Point(l.location.lat, l.location.long)) <= float(radius_km):
                resultset.append(l)
    return resultset

def get_lakes_with_avg_depth_above(lakes: List[Lake], depth_min_m: float):
    """Filter function which removes lakes from resultset that have minimum depth metres below the specified"""
    
    amount_of_skipped = 0
    print(f"len data: {len(lakes)}")
    resultset = []
    for l in lakes:
        if l.depth_average_m is not None:
            print(f"Depth_average: {l.depth_average_m}, depth_min: {depth_min_m}")
            if l.depth_average_m > depth_min_m:
                resultset.append(l)
        else:
            amount_of_skipped+=1
    
    print(f'Skipped {amount_of_skipped} lakes due to now avg. depth information!')
    return resultset

def get_lakes_min_distance_to_closest(lakes: List[Lake], buildings: List[Building], min_distance_km: float):
    """Filter function which returns all lakes with minimum distance to closest building above the specified minimum distance in km"""
    resultset = []
    closest_buildings = []
    lakes = np.array(lakes)
    buildings = np.array(buildings)
    for lake in lakes:
        closest = 1000.0
        
        for building in buildings:
            distance = distance_km_between_points(lake.location, building.location)
            if distance < closest:
                closest = distance
                closest_building = building
            if closest < min_distance_km:
                #no need to continue processing once closest is less than minimum distance
                break
        if closest > min_distance_km:
            resultset.append(lake)
            if closest_building:
                closest_buildings.append((lake, closest_building))
    return (resultset, closest_buildings)

def get_maastolakes_min_distance_to_closest(lakes: List[MaastoLake], buildings: List[Building], min_distance_km: float):
    """Filter function which returns all lakes with minimum distance to closest building above the specified minimum distance in km"""
    print(f"Entered maastolakes with {len(lakes)} lakes, {len(buildings)} buildings")
    resultset = []
    closest_buildings = []
    lakes = np.array(lakes)
    buildings = np.array(buildings)
    for lake in lakes:
        closest = 1000.0
        closest_building = None
        for building in buildings:

            distance = distance_km_between_points(lake.location, building.location)
            if distance < closest:
                closest = distance
                closest_building = building
            if closest < min_distance_km:
                #no need to continue processing once closest is less than minimum distance
                break
        if closest > min_distance_km:
            resultset.append(lake)
            if closest_building:
                closest_buildings.append((lake, closest_building))
    return (resultset, closest_buildings)

def build_map(source: Point, mapboxes: List[MapBox], lakedata: List[Lake | MaastoLake],
              closest_buildings: List[Building], radius_km: float, rapids: List[Rapids], 
              buildings: List[Building], roads_cover_1: List[RoadLine], roads_cover_2: List[RoadLine], swamps: List[Swamp], swamps_1: List[Swamp]):
    """A function used to build the folium map"""
    
    print(f'Building map with {len(lakedata)} lake markers')
    f = FinalMap(source)
    for row in mapboxes:
        for mb in row:
            f.set_map_box(mb,'blue')
   
    f.draw_search_radius(source, radius_km, "red")

    #f.draw_line_to_closest_building(closest_buildings)
    f.draw_buildings(buildings)
    f.draw_rapids(rapids)
    f.draw_outline(swamps, "#6ca88b")
    f.fill_polygon(swamps, "#f5a742", 0.4)
    f.draw_outline(swamps_1, "#eb7734")
    f.fill_polygon(swamps_1, "#eb7734", 0.4)
    f.draw_outline(lakedata, "#2057b0")
    f.draw_outline_dashed(roads_cover_1, "#964B00")
    f.draw_outline(roads_cover_2, "#964B00")
    f.add_draw_tools()
    
    #f.draw_cornerbox(target_box)
    f.set_source_marker(source)
    #f.set_maastolake_markers(lakedata, source)
    
    f.set_mouse_position_tool()
    f.add_search_marker(source, radius_km)
    f.generate_map('testmap.html')    


@timeit
def load_threaded(boxlist, feature):
    """Loads buildings from each mapbox and returns them as a single list, runs threaded to enhance
    performance"""
    def process_box(box, results, thread_num, feature):
        results[thread_num] = box.load(feature)
    results = {}
    threads = []
    for num, b in enumerate(boxlist):
       threads.append(Thread(target=process_box, args=(b, results, num, feature)))
    
    for t in threads:
        t.start()

    for t in threads:
        t.join()
    
    objs = []
    for _, val in results.items():
        objs = objs + val
    return objs

def init() -> Tuple[List[Lake], MapBoxManager]:
    """Initialization function, should be called at startup"""
    #load lake data on startup    
    lake_data = get_lake_data()
    print(f"Started with {len(lake_data)} lakes")
    return lake_data, MapBoxManager(0.25)

def run_one_with_maastolakes(lake_data: List[Lake], mb_manager: MapBoxManager, dist_km: float, source_lat:float,
                             source_long:float, depth_m:float, distance_to_closest_building_km:float, draw_swamps: bool | None,
                             draw_roads: bool | None):
    """Performs a single search, generates map and prints out the execution times of subroutines"""  

    bounding_box = make_bounding_box(Point(source_lat, source_long), dist_km)

    corner_boxes = []
    for p in bounding_box.points:
        corner_boxes.append(mb_manager.get_matching(p))
    
    matrix_relevant_boxes = mb_manager.get_submatrix(corner_boxes[0].matrix_x,
                                                        corner_boxes[1].matrix_x,
                                                        corner_boxes[0].matrix_y,
                                                        corner_boxes[3].matrix_y,)
    
    lake_targets = lakes_in_radius(lake_data, Point(source_lat, source_long), dist_km)     

    lakes_with_avg_depth_info = [l for l in lake_targets if l.depth_average_m is not None]
    lakes_with_max_depth_info = [l for l in lake_targets if l.depth_max_m is not None]    
    
    print(f"Got submatrix")  

    box_list = []
    roadlines = []
    swamps = []
   
    for row in matrix_relevant_boxes:
        for box in row:
            box_list.append(box)
    
    lakes = load_threaded(box_list, 'lake')
    rapids = load_threaded(box_list, 'rapids')
    buildings = load_threaded(box_list, 'building')
    
    if draw_roads:
        roadlines = load_threaded(box_list, 'roadline')
        print(f"Loaded {len(swamps)} swamps")
    
    roads_covering_1 = [r for r in roadlines if r.covering == 1]
    roads_covering_2 = [r for r in roadlines if r.covering == 2]    
    
    
    if draw_swamps:
        swamps = load_threaded(box_list, 'swamp')
        print(f"Loaded {len(swamps)} swamps")

    swamps = get_swamps_in_radius_km(swamps, Point(source_lat, source_long), dist_km)
    
    swamps_non_traversable = [s for s in swamps if s.traversability_class == 2]
    swamps_cls_1 = [s for s in swamps if s.traversability_class == 1]       

    lakes = get_maastolakes_in_radius_km(lakes, Point(source_lat, source_long), dist_km)

    print(f"Got {len(lake_targets)} jarvitieto lakes in radius.")
    print(f"Got {len(lakes_with_avg_depth_info)} lakes with avg depth info.")
    print(f"Got {len(lakes_with_max_depth_info)} lakes with max depth info.")  

    print(f'Got {len(swamps)} in radius')
    print(f"Traversable class 2 swamps: {len(swamps_non_traversable)}")
    print(f"Loaded {len(rapids)} rapids")    
    
    print(f"proceeding to final phase with: {len(lakes)} lakes - {len(buildings)} buildings")
    
    maastolakes, closest_buildings = get_maastolakes_min_distance_to_closest(lakes, buildings, distance_to_closest_building_km)
    build_map(Point(source_lat, source_long), matrix_relevant_boxes, maastolakes,
              closest_buildings, dist_km, rapids, buildings, roads_covering_1, roads_covering_2, swamps_non_traversable, swamps_cls_1)
  


def main():
    
    
    map_box_manager = MapBoxManager(0.25)

    command_params = ['']
    
    while command_params[0] != 'q':
        command_params = input('> ').strip().split(' ')
        match command_params[0]:
            case 'd': #search lakes by distance in km from point, d km lat long depth_m
                if len(command_params) != 6:
                    print(f"Invalid param amount! Should be 6, got {len(command_params)}!")
                    continue
                dist_km = float(command_params[1])
                source_lat = float(command_params[2])
                source_long = float(command_params[3])
                depth_m = float(command_params[4])
                distance_to_closest_building_km = float(command_params[5])
                if not validated(source_lat, source_long, dist_km, distance_to_closest_building_km, depth_m):
                    print("Input params not valid!")
                else:
                    while True:
                        run_one_with_maastolakes(map_box_manager, dist_km, source_lat, source_long, depth_m, distance_to_closest_building_km)
                        rerun = input('rerun? (y) >')
            
                        if rerun == 'y':
                            continue
                        else:
                            break
                
            case default:
                continue   


if __name__ == '__main__':
    main()