import folium

from folium.plugins import Draw
from typing import List
from .map_objects import Point
from .map_matrix import MapBox
from .api_connector import Lake
from .map_objects import Building, Rapids
from .settings import PATH_BASE
from pathlib import Path
from folium.plugins import MousePosition
from .util import distance_km_between_points

class FinalMap():

    def __init__(self, source: Point):
        
        self.m = folium.Map(location=[source.lat, source.long], zoom_start=10, control_scale=True)
        self.tooltip = "Click me"

    
    def set_mouse_coords(self):       
        """Display co-ordinates of mouse position"""
        MousePosition().add_to(self.m)

    def set_mouse_position_tool(self):
        """Display co-ordinates of mouse position with formatted display string"""       

        formatter = "function(num) {return L.Util.formatNum(num, 3) + ' &deg; ';};"

        MousePosition(
            position="topright",
            separator=" | ",
            empty_string="NaN",
            lng_first=True,
            num_digits=20,
            prefix="Coordinates:",
            lat_formatter=formatter,
            lng_formatter=formatter,
        ).add_to(self.m)

    def set_source_marker(self, p: Point):
        """Set the marker for the source point"""
        kw = {"prefix": "fa", "color": "green", "icon": "arrow-up"}
        angle = 180
        icon = folium.Icon(angle=angle, **kw)
        folium.Marker(location=[p.lat, p.long], icon=icon, tooltip=str("Source")).add_to(self.m)


    def set_map_box(self, box: MapBox, color: str):
        """Draw a map"""
        bounds = [[box.lat_range[0], box.long_range[0]], [box.lat_range[1], box.long_range[1]]]
        
        kw = {
            "color": "red",
            "line_cap": "round",
            "fill": True,
            f"fill_color": color,
            "fill_opacity": 0.2,
            "weight": 3,
        }
        folium.Rectangle(
            bounds=bounds,
            line_join="round",
            dash_array="5, 5",
            **kw,
        ).add_to(self.m)

    def set_bounding_box(self, box: List[Point], color: str):
        """Add a bounding box to the map"""
        bounds = [[box[0].lat, box[0].long], [box[3].lat, box[3].long]]
        
        kw = {
            "color": "red",
            "line_cap": "round",
            "fill": True,
            f"fill_color": color,
            "fill_opacity": 0.2,
            "weight": 5,
        }
        folium.Rectangle(
            bounds=bounds,
            line_join="round",
            dash_array="5, 5",
            **kw,
        ).add_to(self.m)

    def draw_search_radius(self, source: Point, radius: float, color: str):

        kw = {
            "color": "red",
            "line_cap": "round",
            "fill": True,
            f"fill_color": color,
            "fill_opacity": 0.2,
            "weight": 3,
        }
         
        loc = [source.lat, source.long]
        folium.Circle(
            location=loc,
            radius=radius*1000,
            line_join="round",
            dash_array="5, 5",
            **kw,
        ).add_to(self.m)

    def draw_line_to_closest_building(self, closest_buildings):
        """Draw a line between a lake and its closest building"""
        for pair in closest_buildings:
            lake = pair[0]
            building = pair[1]
            folium.PolyLine([[lake.location.lat, lake.location.long], [building.location.lat, building.location.long]], color="red", weight=2.0, opacity=1).add_to(self.m)

    def set_lake_markers(self, lakes, source):
        """Set markers for all lakes"""
        for l in lakes:
            color='blue'
            if l.depth_average_m:
                color='pink'
            
            depth = l.depth_average_m or 'NA'
            tooltip = f"Average depth: {depth}\n Direct distance: {distance_km_between_points(source, l.location):.2f}"            
            folium.Marker([l.lat, l.long], popup='<stron>Location 1</strong>', 
                tooltip=tooltip, icon=folium.Icon(icon='cloud', color=color)).add_to(self.m)
    
    def set_maastolake_markers(self, lakes, source):        
        """Set markers for all lakes"""

        color='blue'
        for l in lakes:
                                    
            depth = 'NA'
            tooltip = f"Average depth: {depth}\n Direct distance: {distance_km_between_points(source, l.location):.2f}"            
            folium.Marker([l.location.lat, l.location.long], popup='<stron>Location 1</strong>', 
                tooltip=tooltip, icon=folium.Icon(icon='cloud', color=color)).add_to(self.m)

    def draw_buildings(self, buildings):
        """draw buildings outlines to map"""

        points = []
        for b in buildings:
            for p in b.outline_points:
                points.append([p.lat, p.long])

            folium.PolyLine(points, color='#a020f0', weight=5).add_to(self.m)
            points=[]

    def draw_rapids(self, rapids):
        """draw buildings outlines to map"""
        points = []
        for r in rapids:
            for p in r.outline_points:
                points.append([p.lat, p.long])

            folium.PolyLine(points, color='#ff33e7').add_to(self.m)
            points=[]
    
    def draw_outline(self, swamps, hexcolor: str):
        """draw buildings outlines to map"""
        points = []
        for s in swamps:
            for p in s.outline_points:
                points.append([p.lat, p.long])
            
            folium.PolyLine(points, color=hexcolor).add_to(self.m)
            points=[]
    
    def draw_outline_dashed(self, swamps, hexcolor: str):
        """draw buildings outlines to map"""
        points = []
        for s in swamps:
            for p in s.outline_points:
                points.append([p.lat, p.long])
            
            folium.PolyLine(points, color=hexcolor, dash_array='5').add_to(self.m)
            points=[]

    def fill_polygon(self, objs, color: str, opacity: float):
        kw = {
            "color": color,
            "line_cap": "round",
            "fill": True,
            f"fill_color": color,
            "fill_opacity": opacity,
            "weight": 1,
        }

        points = []
        for o in objs:
            for p in o.outline_points:
                points.append([p.lat, p.long])
        
       
            folium.Polygon(
                locations=points,
                line_join="round",
                dash_array="5, 5",
                **kw,
            ).add_to(self.m)
            points=[]

    def draw_cornerbox(self, points):
        pts = []
        for p in points:
            pts.append([p.lat, p.long])
        folium.PolyLine(pts, color='#141212').add_to(self.m)
            

    def add_search_marker(self, source: Point, radius_km):
        folium.Marker(
            location=[source.lat-0.5, source.long],
            tooltip="NEW SEARCH",
            popup="<a href=http://localhost:8000/search>New search!</a>",
            icon=folium.Icon(color="green"),
        ).add_to(self.m)

    def add_draw_tools(self):
        Draw(export=True).add_to(self.m)


    def generate_map(self, path):
        """saves a .html map using folium"""
        
        p = PATH_BASE / 'ui' / path
        self.m.save(p)

