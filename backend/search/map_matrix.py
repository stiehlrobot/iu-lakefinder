"""contains mapbox and mapboxholder"""
import numpy as np
from pathlib import Path
from typing import List
from .map_objects import Point, Building, MaastoLake, Rapids, Swamp, RoadLine
import threading
from .settings import PATH_BASE
from .util import timeit

class MapBox:

    """A class definition for a mapbox which is a container for buildings in a specific lat-long range"""

    def __init__(self, pos_x:int,  pos_y:int, lat_range: tuple, long_range: tuple):
        self.matrix_x = pos_x
        self.matrix_y = pos_y
        self.lat_range = lat_range
        self.long_range = long_range
        self._root_path = PATH_BASE / 'search' / 'mapbox_data'
        self.fpath_building = self._root_path  / 'rakennusreunaviiva' / '0_25' / f'buildings_{self.matrix_x}_{self.matrix_y}.json'
        self.fpath_lake = self._root_path /  'jarvi' / '0_25' / f'lakes_{self.matrix_x}_{self.matrix_y}.json'
        self.fpath_rapids = self._root_path / 'rapids' / '0_25' / f'rapids_{self.matrix_x}_{self.matrix_y}.json'
        self.fpath_swamp = self._root_path  / 'suo' / '0_25' / f'rapids_{self.matrix_x}_{self.matrix_y}.json'
        self.fpath_swamp = self._root_path  / 'suo' / '0_25' / f'rapids_{self.matrix_x}_{self.matrix_y}.json'

        self._lock = threading.Lock()

    def is_inside(self, data: tuple):
        d_long = data[0]
        d_lat = data[1]
        if self.long_range[0] <= d_long <= self.long_range[1] and self.lat_range[0] <= d_lat <= self.lat_range[1]:
            return True
        return False
    
    def contains(self, p: Point):       
        if self.long_range[0] <= p.long <= self.long_range[1] and self.lat_range[0] <= p.lat <= self.lat_range[1]:
            return True
        return False
    
    def _get_fpath(self, obj):
        root_path = PATH_BASE / 'search' / 'mapbox_data'
        if isinstance(obj, Building):
            return root_path / 'rakennusreunaviiva' / '0_25' / f'buildings_{self.matrix_x}_{self.matrix_y}.json'
        elif isinstance(obj, Rapids):
            return root_path / 'rapids' / '0_25' / f'rapids_{self.matrix_x}_{self.matrix_y}.json'
        elif isinstance(obj, Swamp):
            return root_path / 'suo' / '0_25' / f'swamps_{self.matrix_x}_{self.matrix_y}.json'
        elif isinstance(obj, MaastoLake):
            return root_path / 'jarvi' / '0_25' / f'lakes_{self.matrix_x}_{self.matrix_y}.json'
        elif isinstance(obj, RoadLine):
            return root_path / 'roadline' / '0_25' / f'roadline_{self.matrix_x}_{self.matrix_y}.json'
        else:
            raise TypeError
        
    def _get_feature_fpath(self, feature_name):
        root_path = PATH_BASE / 'search' / 'mapbox_data'
        if feature_name == 'building' or feature_name == 'rakennusreunaviiva':
            return root_path / 'rakennusreunaviiva' / '0_25' / f'buildings_{self.matrix_x}_{self.matrix_y}.json'
        elif feature_name == 'rapids' or feature_name == 'virtaalue':
            return root_path / 'rapids' / '0_25' / f'rapids_{self.matrix_x}_{self.matrix_y}.json'
        elif feature_name == 'swamp' or feature_name == 'suo':
            return root_path / 'suo' / '0_25' / f'swamps_{self.matrix_x}_{self.matrix_y}.json'
        elif feature_name == 'lake' or feature_name == 'jarvi':
            return root_path / 'jarvi' / '0_25' / f'lakes_{self.matrix_x}_{self.matrix_y}.json'
        elif feature_name == 'roadline' or feature_name == 'tieviiva':
            return root_path / 'roadline' / '0_25' / f'roadline_{self.matrix_x}_{self.matrix_y}.json'
        else:
            raise TypeError
        
    def _get_obj(self, feature_name):
        """ TODO This should be faster using a dict or instead of a conditional"""
        if feature_name == 'building' or feature_name == 'rakennusreunaviiva':
            return Building
        elif feature_name == 'rapids' or feature_name == 'virtaalue':
            return Rapids
        elif feature_name == 'swamp' or feature_name == 'suo':
            return Swamp
        elif feature_name == 'lake' or feature_name == 'jarvi':
            return MaastoLake
        elif feature_name == 'roadline' or feature_name == 'tieviiva':
            return RoadLine
        else:
            raise TypeError
        
    def append_building_to_file(self, b: Building):
        with self._lock:
            try:
                with open(self.fpath_building, 'a') as bout:
                    bout.write(b.to_json())
                    bout.write('\n')
            except IOError as err:
                print(err)

    def append_lake_to_file(self, m: MaastoLake):
        with self._lock:
            try:
                with open(self.fpath_lake, 'a+') as bout:
                    bout.write(m.to_json())
                    bout.write('\n')
            except IOError as err:
                print(err)
    
    def append_rapids_to_file(self, r: Rapids):
        with self._lock:
            try:
                with open(self.fpath_rapids, 'a+') as bout:
                    bout.write(r.to_json())
                    bout.write('\n')
            except IOError as err:
                print(err)

    def append_swamp_to_file(self, s: Swamp):
        with self._lock:
            try:
                with open(self.fpath_swamp, 'a+') as bout:
                    bout.write(s.to_json())
                    bout.write('\n')
            except IOError as err:
                print(err)

    def append_map_object_to_file(self, obj: Building | Rapids | Swamp | MaastoLake | RoadLine):
        with self._lock:
            try:
                with open(self._get_fpath(obj), 'a+') as bout:
                    bout.write(obj.to_json())
                    bout.write('\n')
            except IOError as err:
                print(err)
    
    def append_to_file(self, coords):
        try:
            with open(self.fpath, 'a') as bout:
                bout.write(f'{coords}\n')
        except IOError as err:
            print(err)

    def load_lakes(self):
        data_items = []
        if not self.fpath_lake.is_file:
            raise FileNotFoundError
        try:
            with open(self.fpath_lake, 'r') as fin:
                for line in fin:
                    b = MaastoLake.from_json(line)
                    data_items.append(b)

        except IOError as err:
            print(err)
        return data_items


    def load_buildings(self):
        data_items = []
        if not self.fpath_building.is_file:
            raise FileNotFoundError
            
        try:
            with open(self.fpath_building, 'r') as fin:
                for line in fin:
                    b = Building.from_json(line)
                    data_items.append(b)

        except IOError as err:
            print(err)
        return data_items
    
    def load_rapids(self):
        data_items = []
        if not self.fpath_rapids.is_file:
            raise FileNotFoundError
            
        try:
            with open(self.fpath_rapids, 'r') as fin:
                for line in fin:

                    b = Rapids.from_json(line)
                    data_items.append(b)

        except IOError as err:
            print(err)
        return data_items
    
    def load_swamps(self):
        data_items = []
        if not self.fpath_swamp.is_file:
            raise FileNotFoundError
        try:
            with open(self.fpath_swamp, 'r') as fin:
                for line in fin:
                    b = Swamp.from_json(line)
                    data_items.append(b)

        except IOError as err:
            print(err)
        return data_items
    
    def load(self, feature):        
        data_items = []
        if not self._get_feature_fpath(feature).is_file:
            return data_items
        try:
            with open(self._get_feature_fpath(feature), 'r') as fin:
                for line in fin:
                    
                    b = self._get_obj(feature).from_json(line)
                    data_items.append(b)

        except IOError as err:
            print(err)
        return data_items


    def __repr__(self) -> str:
        return f"matrix pos: [{self.matrix_x}] [{self.matrix_y}] , lat_range: {self.lat_range}, long_range: {self.long_range}"

def generate_mb_matrix(bound_increment: float):
    """Generates mapboxes from left to right, row by row, returns a matrix"""

    print(f"Generating mapbox matrix with bound increment: {bound_increment}")
    GPS_PRECISION = 0.0000001
    

    #These are more or less the lat / longitude ranges of a rectangle drawn at the boundaries of Finland
    LONG_MIN = 19.5
    LONG_MAX = 31.5
    LAT_MIN = 59.0
    LAT_MAX = 70.0
       
    long_bounds = []
    lat_bounds = []

    bound_increment_long = bound_increment
    bound_increment_lat = bound_increment_long / 2
    for i in np.arange(LONG_MIN, LONG_MAX, bound_increment_long):
        long_bounds.append(i)
    for j in np.arange(LAT_MIN, LAT_MAX, bound_increment_lat):
        lat_bounds.append(j)
    matrix_pos_x = 0
    matrix_pos_y = 0

    #initialize the matrix with None
    mb_matrix = np.full((len(long_bounds), len(lat_bounds)), None)
 
    #populate matrix with mapboxes
    for l in lat_bounds:
        for ll in long_bounds:
            lat_low = l + GPS_PRECISION
            lat_high = l + bound_increment_lat
            long_low = ll + GPS_PRECISION
            long_high = ll + bound_increment_long
            #print(f"Created mapbox with: long: {long_low} - {long_high} lat: {lat_low} - {lat_high} at matrix pos: [{matrix_pos_x}, {matrix_pos_y}]")
            
            #mapboxes.append(MapBox((matrix_pos_x, matrix_pos_y), (lat_low, lat_high),(long_low, long_high)))
            mb_matrix[matrix_pos_x, matrix_pos_y] = MapBox(matrix_pos_x, matrix_pos_y, (lat_low, lat_high),(long_low, long_high))
            matrix_pos_x += 1
        matrix_pos_y += 1
        matrix_pos_x = 0
    print(f"Generated {len(long_bounds)} by {len(lat_bounds)} matrix")
    return mb_matrix


class MapBoxManager:

    """This class contains a matrix of mapboxes and provides access functions"""

    def __init__(self, bound_increment=0.25):
        self.matrix = generate_mb_matrix(bound_increment)

    def get_box_by_matrix_pos(self, x: int, y: int):
        """Return a mapbox at a specified matrix position"""
        try:
            return self.matrix[x][y]
        except IndexError:
            return None

    def get_submatrix(self, x_min: int, x_max: int, y_min: int, y_max: int):
        """Return a submatrix in specified bounds"""
        return self.matrix[x_min:x_max+1, y_min:y_max+1]
    
    def get_matching(self, p: Point) -> MapBox | None:
        """Returns the mapbox that contains a Point"""
        for row in self.matrix:
            for mb in row:
                if mb.contains(p):
                    return mb
        return None
    
    def get_n_outer_layer_boxes(self, core_mapbox: MapBox, layers_from_core: int):
        """In a matrix, returns n layers from the centremost block"""
        mapboxes = []
        x_val, y_val = core_mapbox.matrix_pos
        for layer in range(1, layers_from_core):
            mapboxes + self.get_mapbox_layer(x_val, y_val, layer)

    def get_mapbox_layer(self, core_x: int, core_y: int, n):

        mb_list = []

        pos_x = core_x
        pos_y = core_y

        #jump to top right for start
        pos_x = pos_x + n
        pos_y = pos_y + n               
    

        #1. decrease x_pos by 1, n * 2 + 1 times
        for k in range(1, n*2+1):
            mb_list.append(self.get_box_by_matrix_pos(pos_x-k, pos_y))

        #2. decrease y_pos by 1, n * 2 + 1 times
        for k in range(1, n*2+1):
            mb_list.append(self.get_box_by_matrix_pos(pos_x, pos_y-k))

        #3. increase x_pos by 1, n * 2 + 1 times
        for k in range(1, n*2+1):
            mb_list.append(self.get_box_by_matrix_pos(pos_x+k, pos_y))

        #4. increase y_pos by 1, n*2 times        
        for k in range(1, n*2+1):
            mb_list.append(self.get_box_by_matrix_pos(pos_x+k, pos_y))

        return [m for m in mb_list if m is not None]