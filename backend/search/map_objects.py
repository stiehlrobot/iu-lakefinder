"""Contains map object data class definitions"""

from dataclasses import dataclass
from dataclasses_json import dataclass_json
from typing import List

@dataclass
class Point:
    """Used to store gps coordinates (lat, long)"""
    lat: float
    long: float

@dataclass_json
@dataclass
class Swamp:
    """
    Used to store a swamp:
    - gps location as Point
    - outline as list of points
    - traversability
    """
    location: Point
    outline_points: List[Point]
    traversability_class: int
    forest_class: int

@dataclass_json
@dataclass
class Building:
    """Used to store a building: 
    - gps location as a Point
    - outline of the building as a list of gps  """
    location: Point
    outline_points: List[Point]

@dataclass_json
@dataclass
class RoadLine:
    """Used to store a roadline: 
    - outline of linestring points
    - traverse method
    - covering
    - one-directional
    - name finnish"""
    location: Point
    outline_points: List[Point]
    traverse_method: int
    covering: int
    one_directional: int
    name_finnish: str


@dataclass_json
@dataclass
class Rapids:
    """Used to store a building: 
    - gps location as a Point
    - outline of the building as a list of gps  """
    location: Point
    outline_points: List[Point]

@dataclass
class Square:
    """
    Used to store a list of points that form a square
    """

    points: List[Point]

@dataclass_json
@dataclass
class MaastoLake:
    """Used to store a lake from Maasto database"""
    location: Point
    outline_points: List[Point]


@dataclass_json
@dataclass
class Lake:
    """
    Used to store a lake
    """

    id: int | None = None
    number: str | None = None
    name: str | None = None
    location: Point | None = None
    lat: str | None = None
    long: str | None = None
    municipality_name: str | None = None
    water_area_size_ha: str | None = None
    coastline_length: str | None = None
    depth_average_m: str | None = None
    depth_max_m: str | None = None
    water_volume_m3: str | None = None
    depth_max_lat: str | None = None
    depth_max_long: str | None = None