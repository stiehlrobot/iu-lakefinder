"""Utility functions"""

from math import sin, cos, sqrt, atan2, radians
from .map_objects import Point, Square, Lake
from typing import List
from functools import wraps
import time



def timeit(func):
    """Copied from here https://dev.to/kcdchennai/python-decorator-to-measure-execution-time-54hk"""
    @wraps(func)
    def timeit_wrapper(*args, **kwargs):
        start_time = time.perf_counter()
        result = func(*args, **kwargs)
        end_time = time.perf_counter()
        total_time = end_time - start_time
        print(f'Function {func.__name__}{args} {kwargs} Took {total_time:.4f} seconds')
        return result
    return timeit_wrapper


def validated(lat, long, search_radius_km, distance_to_closest_km, min_lake_depth):
    validated = True
    try:
        if not -90 <= float(lat) <= 90:
            validated = False
        if not -180 <= float(long) <= 180:
            validated = False
        if not 0.0 <= float(search_radius_km) <= 100.0:
            validated = False
        if not 0.0 <= float(distance_to_closest_km) <= 20.0:
            validated = False
        if not 0.0 <= float(min_lake_depth) <= 15.0:
            validated = False
    except ValueError:
        return False

    return validated

def distance_between_km(source: tuple[str, str], target: tuple[str, str]) -> float:
    src_lat, src_long = source
    target_lat, target_long = target
    try:
        src_lat = float(src_lat)
        src_long = float(src_long)
        target_lat = float(target_lat)
        target_long = float(target_long)

    except ValueError as err:
        SystemExit(err)

    assert(-90 <= src_lat <= 90)
    assert(-90 <= target_lat <= 90)
    assert(-180 <= src_long <= 180)
    assert(-180 <= target_long <= 180)

    # Approximate radius of earth in km
    R = 6373.0

    lat1 = radians(src_lat)
    lon1 = radians(src_long)
    lat2 = radians(target_lat)
    lon2 = radians(target_long)

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    distance = R * c

    return distance

def get_average(coords):
    """Return lat, long averages for a list of coordinates"""
    
    sum_lats=0.0
    sum_longs=0.0
    avg_lat=0.0
    avg_long=0.0
    for c in coords:
        long, lat = tuple(c)
        
        sum_lats+=lat
        sum_longs+=long
        avg_lat = sum_lats / len(coords)
        avg_long = sum_longs / len(coords)
    return avg_lat, avg_long


def distance_km_between_points(src: Point, target: Point) -> float:
    """Returns a distance in km between two points"""
    
    """
    assert(-90 <= src.lat <= 90)
    assert(-90 <= target.lat <= 90)
    assert(-180 <= src.long <= 180)
    assert(-180 <= target.long <= 180)
    """

    # Approximate radius of earth in km
    R = 6373.0

    lat1 = radians(src.lat)
    lon1 = radians(src.long)
    lat2 = radians(target.lat)
    lon2 = radians(target.long)

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))
    
    return R * c

def distance_km_between_points_threaded(source: Point, target: Point) -> float:
    """Return distance in km between two points"""
    s_lat = float(source.lat)
    s_long = float(source.long)
    t_lat = float(target.lat)
    t_long = float(target.long)
  
    assert(-90 <= s_lat <= 90)
    assert(-90 <= t_lat <= 90)
    assert(-180 <= s_long <= 180)
    assert(-180 <= t_long <= 180)

    # Approximate radius of earth in km
    R = 6373.0

    lat1 = radians(s_lat)
    lon1 = radians(s_long)
    lat2 = radians(t_lat)
    lon2 = radians(t_long)

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    distance = R * c

    return distance


def make_bounding_box(p: Point, box_radius_km: float) -> Square:
    """Build a bounding box with a given radius in km centered on a point"""

    #formulas for average longitude and latitude per km in Finland
    AVG_LONG_PER_KM = 1.0 / 40.0
    AVG_LAT_PER_KM = 1.0 / 111

    #convert box_radius_km to lat
    p1_long = p.long - (box_radius_km * AVG_LONG_PER_KM) #bottom left point of box
    p1_lat = p.lat - (box_radius_km * AVG_LAT_PER_KM)

    p2_long = p.long + (box_radius_km * AVG_LONG_PER_KM) #bottom right point of box
    p2_lat = p.lat - (box_radius_km * AVG_LAT_PER_KM)

    p3_long = p.long - (box_radius_km * AVG_LONG_PER_KM) #top left point of box
    p3_lat = p.lat + (box_radius_km * AVG_LAT_PER_KM)

    p4_long = p.long + (box_radius_km * AVG_LONG_PER_KM) #top right point of box
    p4_lat = p.lat + (box_radius_km * AVG_LAT_PER_KM)

    return Square([Point(p1_lat, p1_long), Point(p2_lat, p2_long), Point(p3_lat, p3_long), Point(p4_lat, p4_long)])

def lakes_in_radius(lakedata: List[Lake], source: Point, radius_km: float):
    return [l for l in lakedata if distance_km_between_points(Point(l.lat, l.long), source) <= radius_km]
