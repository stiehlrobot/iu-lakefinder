"""Combines all chunks of lakedata into a single .json"""


import glob
import json

# Define the path pattern
path_pattern = "lakedata*.json"  # Example pattern, adjust as needed

# Use glob to match files based on the pattern
matching_files = glob.glob(path_pattern)

# Print the matched files
print("Matching files:")
save_path='lakedata_combined.json'
for file_path in matching_files:

    # Open the JSON file and load its contents
    with open(file_path, "r") as json_file:
        data = json.load(json_file)

        # Now 'data' contains the contents of the JSON file
        i = 0
        while True:
            try:
                with open(save_path, 'a') as wout:
                    wout.write(json.dumps(data['value'][i]))
                    wout.write('\n')
                
            except IndexError as err:
                print("Breaking out")
                break
            i+=1
    
