import requests
from map_objects import Point
import folium
import json
"""
gdata = {
  "type": "FeatureCollection",
  "features": [
    {
      "type": "Feature",
      "properties": {},
      "geometry": {
        "coordinates": [
          [
            24.89985250223492,
            60.23549840572025
          ],
          [
            24.877545394082404,
            60.26030179126684
          ],
          [
            24.874757325683532,
            60.26820575262582
          ],
          [
            24.869974690925858,
            60.27669999501674
          ],
          [
            24.85723962483752,
            60.293182244924225
          ],
          [
            24.878743613622362,
            60.29801844938211
          ],
          [
            24.89905530730519,
            60.29861140478897
          ],
          [
            24.911796359407305,
            60.29860731712975
          ]
        ],
        "type": "LineString"
      }
    }
  ]
}
"""

gdata2= """
"geometry":{"coordinates":[[60.42074,25.298554],[60.42074,25.298554]],"type":"LineString"}}],"waypoints":[{"distance":57174.198,"name":"","location":[60.42074,25.298554]},{"distance":58101.653,"name":"","location":[60.42074,25.298554]}],"code":"Ok","uuid":"G5EiIZi7xTHFfYyk7urIWVPnKIdoSMdNIt6fZ_9nDYc2s_Nx9k5Xmg=="}
"""

class MapBoxClient:

    """
    https://api.mapbox.com/directions/v5/mapbox/cycling/-84.518641,39.134270;-84.512023,39.102779?geometries=geojson&access_token= <UserAccessToken />
    """

    def __init__(self):
        self.api_key = "pk.eyJ1IjoidXVsYXJpIiwiYSI6ImNsdmppc2hqczFtbmMycW80ZjUwNjEzZmQifQ.XYzg2jfo530QJh1z-EUj_g"
        self.path_base = "https://api.mapbox.com/directions/v5/mapbox/"


    def get_route_as_geojson(self, start: Point, dest: Point, travel_type: str):
        assert(travel_type in ("driving", "driving-traffic", "cycling", "walking"))
           
        query_path = self.path_base + travel_type + '/' + f'{start.lat},{start.long};{dest.lat},{dest.long}?geometries=geojson&access_token={self.api_key}'
        print(query_path)
        try:
            resp = requests.get(query_path)
        except requests.exceptions.Timeout:
            print("Error: Got timeout")
        except requests.exceptions.TooManyRedirects:
            print("Bad url")
        except requests.exceptions.RequestException as e:
            raise SystemExit(e)
        return resp.text
    
mb = MapBoxClient()
resp = mb.get_route_as_geojson(Point(60.154636, 24.921588), Point(60.181348, 24.926092), 'driving')
print(resp)
m = folium.Map(
    location=(60.181348, 24.926092),
    control_scale=True,
)
jso = json.loads(resp)
print('-'*40)
data = jso['routes'][0]['geometry']
print(data)



gdata = {
  "type": "FeatureCollection",
  "features": [
    {
      "type": "Feature",
      "properties": {},
      "geometry": {
        "coordinates": [
          [
            24.89985250223492,
            60.23549840572025
          ],
          [
            24.877545394082404,
            60.26030179126684
          ],
          [
            24.874757325683532,
            60.26820575262582
          ],
          [
            24.869974690925858,
            60.27669999501674
          ],
          [
            24.85723962483752,
            60.293182244924225
          ],
          [
            24.878743613622362,
            60.29801844938211
          ],
          [
            24.89905530730519,
            60.29861140478897
          ],
          [
            24.911796359407305,
            60.29860731712975
          ]
        ],
        "type": "LineString"
      }
    }
  ]
}
print("before:")
print(gdata)
gdata['features'][0]['geometry'] = data
print('-'*50)
print("after")
print(gdata)


folium.GeoJson(gdata).add_to(m)

m.save('testgeojson.html')