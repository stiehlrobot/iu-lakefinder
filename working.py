'''
@app.post("/search", response_class=HTMLResponse)
async def search(request: Request, gpsLat: str = Form(...),
                 gpsLong: str = Form(...),
                 searchRadius: str = Form(...),
                 distanceToClosestBuilding: str = Form(...),):
    global lake_data, mb_manager
    
    if validated(gpsLat, gpsLong, searchRadius, distanceToClosestBuilding):
        run_one(lake_data, mb_manager, float(searchRadius), float(gpsLat), float(gpsLong), 0.0, float(distanceToClosestBuilding))
        return templates.TemplateResponse("index.html", context={"request": request})
    else:
        redirect_url = str(request.url_for('index'))+ '?x-error=Invalid+params'
        return RedirectResponse(redirect_url, status_code=status.HTTP_302_FOUND, headers={"x-error": "Invalid search params"})
'''

import folium

m = folium.Map(location=(45.5236, -122.6750))
m.save("spandex.html")

'''
<form>
                    <div class="form-group">
                      <label for="formControlRange">Example Range input</label>
                      <input type="range" class="form-control-range" id="formControlRange">
                    </div>
                  </form>
'''